package com.digipet.backend;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import com.digipet.backend.repositories.PetRepo;
import com.digipet.backend.models.Pet;
import com.digipet.backend.models.User;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
class PetRepoTests {

    @MockBean
    private PetRepo mockPetRepo;

    @Test
    void shouldSavePet(){
        Pet pet = new Pet("Miffy");
        mockPetRepo.save(pet);
        when(mockPetRepo.findByName(pet.getName())).thenReturn(pet);
        assertEquals(pet.getName(), mockPetRepo.findByName(pet.getName()).getName());
    }

    @Test 
    void shouldFindByPetname(){
        Pet pet1 = new Pet("Hansel");
        mockPetRepo.save(pet1);
        Pet pet2 = new Pet("Gretel");
        mockPetRepo.save(pet2);
        List<Pet> pets = new ArrayList<Pet>();
        pets.add(pet1);
        pets.add(pet2);
        when(mockPetRepo.findAll()).thenReturn(pets);
        assertEquals(2, pets.size());
    }
    @Test
    void shouldFindPetByOwner(){
        Pet pet = new Pet("Mario");
        User owner = new User("Momma", "123");
        pet.setOwner(owner);
        mockPetRepo.save(pet);
        //Optional<Pet> p = Optional.of(pet);
        when(mockPetRepo.findByOwner(pet.getOwner())).thenReturn(pet);
        //Pet petfound = p.get(); // get method retrieves the object if it exist instead of Optional<object>
        assertEquals("Momma", pet.getOwner().getUsername());
    }

    @Test // what is the return value of spring boot deletebyID -> google it
    void shouldDeletePet() {
        Pet pet = new Pet("Tommy");
        pet.setId(2L);
        mockPetRepo.save(pet);
        mockPetRepo.deleteById(2L);
        when(mockPetRepo.existsById(pet.getId())).thenReturn(false);
        assertFalse(mockPetRepo.existsById(pet.getId()));
    }

	@Test
    void shouldUpdatepetData(){
        Pet pet = new Pet("Jackie");
        mockPetRepo.save(pet);
        String newName = "Joe";
        pet.setName(newName);;
        mockPetRepo.save(pet);
        when(mockPetRepo.findByName(pet.getName())).thenReturn(pet);
        assertEquals(newName, pet.getName()); 
    }
}

 