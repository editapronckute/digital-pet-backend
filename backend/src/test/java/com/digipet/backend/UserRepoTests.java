package com.digipet.backend;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.digipet.backend.repositories.UserRepo;
import com.digipet.backend.models.User;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;


@SpringBootTest
class UserRepoTests {

    @MockBean
    private UserRepo mockUserRepo;

    @Test
    void shouldcreateUser() {
        User user = new User("John Doe", "Pass123");
        mockUserRepo.save(user);
        when(mockUserRepo.findByUsername(user.getUsername())).thenReturn(user);
        assertEquals(user.getUsername(), mockUserRepo.findByUsername(user.getUsername()).getUsername());
    }

    @Test
    void shouldReturnAllUsers() {
        User user1 = new User("Adam", "123");
        mockUserRepo.save(user1);
        User user2 = new User("Eve", "123");
        mockUserRepo.save(user2);
        List<User> users = new ArrayList<User>();
        users.add(user1);
        users.add(user2);
        when(mockUserRepo.findAll()).thenReturn(users);
        assertEquals(2, users.size());
    }

    @Test
    void shouldFindUserById(){
        User user = new User ("Mario", "SuperBros");
        user.setId(1L);
        mockUserRepo.save(user);
        Optional<User> u = Optional.of(user);
        when(mockUserRepo.findById(user.getId())).thenReturn(u);
        User userfound = u.get(); // get method retrieves the object if it exist instead of Optional<object>
        assertEquals(1L, userfound.getId());
    }

    @Test // what is the return value of spring boot deletebyID -> google it
    void shouldDeleteUser() {
        User user1 = new User("Tom", "pass123");
        user1.setId(2L);
        mockUserRepo.save(user1);
        when(mockUserRepo.existsById(user1.getId())).thenReturn(false);
        assertEquals(false, mockUserRepo.existsById(user1.getId()));
    }

    @Test
    void shouldUpdateUser() {
        User user = new User("Zelda", "princess");
        mockUserRepo.save(user);
        String newUsername = "Xena";
        user.setUsername(newUsername);
        mockUserRepo.save(user);
        when(mockUserRepo.findByUsername(user.getUsername())).thenReturn(user);
        assertEquals(newUsername, user.getUsername()); 
    }

}
