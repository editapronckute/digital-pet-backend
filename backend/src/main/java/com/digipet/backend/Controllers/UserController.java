package com.digipet.backend.controllers;
import java.util.List;

import com.digipet.backend.repositories.UserRepo;
import com.digipet.backend.models.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PutMapping;


@RestController
@CrossOrigin
public class UserController {
    
    @Autowired
    UserRepo userRepo;

    //get all users
    @GetMapping("/users")
    public List<User> userlist(){
        return (List<User>) userRepo.findAll();
    }

    //get a user by id
    @GetMapping("/users/{id}")
    public User one(@PathVariable Long id) {
        return userRepo.findById(id).orElseThrow(() -> new UserNotFoundException(id));
    }

    //create a user
    @PostMapping("/users")
    public User newUser(@RequestBody User newUser){
         return userRepo.save(newUser);
    }

    //update user details
    @PutMapping(value="path/{id}")
    public User replaceUser(@RequestBody User newUser, @PathVariable Long id){
        return userRepo.findById(id).map(User -> {
            User.setPassword(newUser.getPassword());
        return userRepo.save(User);
    })
    .orElseGet(() -> {
        return userRepo.save(newUser);
    }); 
    }
    
    //remove user
    @DeleteMapping("/users/{id}")
    public void deleteUser(@PathVariable Long id) {
        userRepo.deleteById(id);
    }
}
