package com.digipet.backend.controllers;

public class PetNotFoundException extends RuntimeException{
    
    private static final long serialVersionUID = 1L;
 
    PetNotFoundException(Long id) {
        super("Could not find Pet " + id);
      }
    
}
