package com.digipet.backend.controllers;

import java.util.List;
import java.util.Optional;

import com.digipet.backend.repositories.PetRepo;
import com.digipet.backend.models.Pet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;


@RestController
@CrossOrigin
public class PetController {
    
    @Autowired
    PetRepo petRepo;
    
    @PostMapping("/pets")
    public Pet newPet(@RequestBody Pet newPet){
        return petRepo.save(newPet);
    }
    @GetMapping("/pets")
    public List<Pet> petlist(){
        return (List<Pet>) petRepo.findAll();
    }

    @GetMapping("/pets/{id}")
    public Pet petFindById(@PathVariable Long id) {

        return petRepo.findById(id)
        .orElseThrow(() -> new PetNotFoundException(id));
    }

    @PutMapping("/pets/{id}")
    public Pet replacePet(@RequestBody Pet newPet, @PathVariable Long id) {
        Optional<Pet> petFound = petRepo.findById(id);
        Pet pet2 = petFound.isPresent()?petFound.get():null;
        pet2.setName(newPet.getName());
        pet2.setAge(newPet.getAge());
        pet2.setState(newPet.getState());
        pet2.setMood(newPet.getMood());

        return petRepo.save(pet2);
    } 
    
    @DeleteMapping("/pets/{id}")
    public void deletePet(@PathVariable Long id) {
        petRepo.deleteById(id);
    }
}
