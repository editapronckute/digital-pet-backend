package com.digipet.backend.models;

public enum State {
    DEAD,
    ALIVE
}
