package com.digipet.backend.models;

public enum Mood {
    HAPPY,
    HUNGRY,
    SAD,
    DIRTY,
    MISERABLE
}
