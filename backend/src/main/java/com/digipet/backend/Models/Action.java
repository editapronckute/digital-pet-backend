package com.digipet.backend.models;

public interface Action {
    public void feed();
    public void snuggle();
    public void bathe();
}
