package com.digipet.backend.repositories;

import com.digipet.backend.models.Pet;
import com.digipet.backend.models.User;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PetRepo extends CrudRepository<Pet, Long> {

  Pet findByOwner (User owner);

  Pet findByName(String string);

}